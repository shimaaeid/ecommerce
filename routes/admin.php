<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
define('PAGINATION_COUNT',10);

Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {

    Route::get('/', 'DashboardController@index')->name('admin.dashboard');

    ############ Begin Languages Routes #########################
    Route::group(['prefix' => 'languages'], function(){

        Route::get('/','LanguageController@index')->name('admin.language');
        Route::get('create' , 'LanguageController@create')->name('admin.language.create');
        Route::post('store', 'LanguageController@store')->name('admin.language.store');
        Route::get('edit/{id}', 'LanguageController@edit')->name('admin.language.edit');
        Route::post('update/{id}', 'LanguageController@update')->name('admin.language.update');
        Route::get('delete/{id}', 'LanguageController@destory')->name('admin.language.delete');
    });
    ############ End Languages Routes ###########################

    ############ Begin MainCagegory Routes #########################
    Route::group(['prefix' => 'main_categories'], function () {

        Route::get('/', 'MainCategoryController@index')->name('admin.maincategories');
        Route::get('create', 'MainCategoryController@create')->name('admin.maincategories.create');
        Route::post('store', 'MainCategoryController@store')->name('admin.maincategories.store');
        Route::get('edit/{id}', 'MainCategoryController@edit')->name('admin.maincategories.edit');
        Route::post('update/{id}', 'MainCategoryController@update')->name('admin.maincategories.update');
        Route::get('delete/{id}', 'MainCategoryController@destory')->name('admin.maincategories.delete');
    });
    ############ End MainCategory Routes ###########################


});


Route::group(['namespace' => 'Admin' , 'middleware' => 'guest:admin'] , function(){

    Route::get('login' , 'LoginController@getLogin');
    Route::post('login' , 'LoginController@Login')->name('admin.login');


});

