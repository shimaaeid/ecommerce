<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    use AuthenticatesUsers;
    public function getLogin(){

        return view('admin.auth.login');
    }

    // public function save()
    // {

    //     $admin = new Admin();
    //     $admin->name = "Shimaa Eid";
    //     $admin->email = "shimaa@gmail.com";
    //     $admin->password = Hash::make('123456');
    //     $admin->save();
    //     return 'saved';
    // }

    public function Login(LoginRequest $request){

        $remember_me = $request->has('remember_me') ? true : false;

        if(auth()->guard('admin')->attempt(['email'=> $request->input('email'),'password'=> $request->input('password')], $remember_me)){

            return redirect()->route('admin.dashboard');
        }
        return redirect()->back()->with(['error' => 'هناك خطا بالبيانات']);



    }


}
