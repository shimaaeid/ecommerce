<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MainCategoryRequest;
use App\Models\MainCategory;
use Exception;
use DB;

class MainCategoryController extends Controller
{

    public function index(){

        $default_lang = get_default_lang();
        $categories = MainCategory::where('translation_lang', $default_lang)->select()->get();

        return view('admin.maincategory.index',compact('categories'));

    }

    public function create(){

        return view('admin.maincategory.create');


    }

    public function store(MainCategoryRequest $request){


        try{
        $main_categories = collect($request->category);
       
        $filterr = $main_categories->filter(function($value){

           return $value['abbr'] == get_default_lang();
        
        });

        $default_category = array_values($filterr->all())[0];
       
        $filePath = '';
        if($request->has('photo')){
            $filePath = upload_post_image('maincategories', $request->photo);
        } 

        DB::beginTransaction();

        $default_category_id = MainCategory::insertGetId([

            'translation_lang' => $default_category['abbr'],
            'translation_of' => 0,
            'name' => $default_category['name'],     
            'slug' => $default_category['name'],
            'photo' => $filePath,
        ]);

    $categories = $main_categories->filter(function ($value, $key) {

            return $value['abbr'] != get_default_lang();
        });

        if(isset($categories) && $categories->count()){
            $categories_arr = [];
            foreach($categories as $category){

                $categories_arr[] = [
                    'translation_lang' => $category['abbr'],
                    'translation_of' => $default_category_id,
                    'name' => $category['name'],     
                    'slug' => $category['name'],
                    'photo' => $filePath,
                ];
            }

            MainCategory::insert($categories_arr);

        }

        DB::commit();
        return redirect()->route('admin.maincategories')->with(['success' => 'تمت الاضافه بنجاح']);

    }catch(\Exception $e){
             DB::rollBack();
            return redirect()->route('admin.maincategories')->with(['error' => 'حدث خطأ ما برجاء المحاوله لاحقا']);

    }

    }


    public function edit($mainCat_id){
        $main_category = MainCategory::select()->find($mainCat_id);

        if(!$main_category )
          return redirect()->route('admin.maincategories')->with(['error' => 'هذا القسم غير موجود']);


        return view('admin.maincategory.edit', compact('main_category'));
    }

    public function update($mainCat_id, MainCategoryRequest $request ){


        $main_category = MainCategory::find($mainCat_id);

        if (!$main_category)
            return redirect()->route('admin.maincategories')->with(['error' => 'هذا القسم غير موجود']);


        $category = array_values($request->category)[0];

        $main_category = MainCategory::where('id', $mainCat_id)->update([
            'name'=> $category['name'],
        ]);
        return redirect()->route('admin.maincategories')->with(['success' => 'تم التحديث بنجاح']);

    }
}
