<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LangaugeRequest;
use App\Models\Language;

class LanguageController extends Controller
{
    public function index(){
       
        $language = Language::select()->paginate(PAGINATION_COUNT);

        return view('admin.languages.index',compact('language'));
    }

    public function create()
    {

        return view('admin.languages.create');
    }

    public function store(LangaugeRequest $request){

        try{

            $data = $request->except(['_token']);

            Language::create($data);
            
            return redirect()->route('admin.language')->with(['success'=> 'تم إضافه اللغه بنجاح']);

        }catch(\Exception $e){

            return redirect()->route('admin.language')->with(['error' => 'هناك خطأ يرجى المحاوله فيما بعد']);
        }
    }

    public function edit($id){

        $language = Language::select()->find($id);
        if(! $language){
            return redirect()->route('admin.langauage')->with(['error' => ' هذه اللغه غير موجوده']);
        }

        return view('admin.languages.edit',compact('language'));

    }

    public function update($id , LangaugeRequest $request ){

        try{

            $language = Language::find($id);
            if (!$language) {
                return redirect()->route('admin.langauage.edit', $id)->with(['error' => ' هذه اللغه غير موجوده']);
            }

            $data = $request->except(['_token']);

            if (!isset($request->active)) {
                $data['active'] = 0;
            }

            $language->update($data);

            return redirect()->route('admin.language')->with(['success' => ' تم تحديث البيانات بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->route('admin.language')->with(['error' => 'هناك خطأ يرجى المحاوله فيما بعد']);

        }

    }

    public function destory($id){

        try {

            $language = Language::find($id);
            if (!$language) {
                return redirect()->route('admin.langauage')->with(['error' => ' هذه اللغه غير موجوده']);
            }
            $language->delete();

            return redirect()->route('admin.language')->with(['success' => ' تم حذف البيانات بنجاح']);
        } catch (\Exception $e) {
            return redirect()->route('admin.language')->with(['error' => 'هناك خطأ يرجى المحاوله فيما بعد']);
        }

    }
}
