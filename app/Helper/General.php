<?php

use App\Models\Language;
use Illuminate\Support\Facades\Config;



function get_language(){

    return Language::active()->select()->get();
}

function get_default_lang(){

    return Config::get('app.locale');

}

 function upload_post_image($folder, $image)
    {
    $image->store('/', $folder);
    $filename = $image->hashName();
    $path = 'images/' . $folder . '/' . $filename;
    return $path;

    }